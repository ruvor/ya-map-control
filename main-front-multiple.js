ymaps.ready(function () {
    var marks = [];
    for (var i in objectsCoords) {
        marks[i] = new ymaps.Placemark(objectsCoords[i]);
    }
    var clusterer = new ymaps.Clusterer();
    clusterer.add(marks);

    var center = window["center"];
    var zoom = window["zoom"];
    var ymap;
    var mapState = {
        controls: ["smallMapDefaultSet"]
    };
    if (center && zoom) {
        mapState.center = center;
        mapState.zoom = zoom;
        ymap = new ymaps.Map("ymap", mapState);
    }
    else {
        var marksBounds = clusterer.getBounds();
        mapState.center = ymaps.util.bounds.getCenter(marksBounds);
        mapState.zoom = 0;
        ymap = new ymaps.Map("ymap", mapState);
        ymap.setBounds(marksBounds, { checkZoomRange: true });
    }
    ymap.geoObjects.add(clusterer);
});