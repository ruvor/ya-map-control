ymaps.ready(function () {
    var ymapControls = document.querySelectorAll(".ymap-control");
    for (var i = 0; i < ymapControls.length; i++) {
        var control = ymapControls[i];
        var lat = control.querySelector(".coord-lat").value;
        var long = control.querySelector(".coord-long").value;
        var mapCenterPromise; //промис, разрешаемый координатами объекта, который принимается за центр карты
        var objectFound = false;
        if (isCoordCorrect(lat) && isCoordCorrect(long)) {
            //координаты уже заполнены и корректны
            objectFound = true;
            var deferred = new ymaps.vow.Deferred();
            deferred.resolve(); //нет необходимости передавать координаты, в этом случае они не будут использоваться
            mapCenterPromise = deferred.promise();
        }
        else {
            //координат нет или они некорректны, нужно определить местоположение пользователя
            control.querySelector(".coord-lat").value = ""; //сброс имеющихся значений на слу-
            control.querySelector(".coord-long").value = ""; //чай, что они некорректны
            mapCenterPromise = getUserPositionObject();
        }
        (function (control, lat, long, objectFound) {
            mapCenterPromise.then(function (centerObject) {
                //создание карты
                var container = control.querySelector(".ymap");
                var mapState;
                if (objectFound)
                {
                    //объект готов, карту надо отображать вокруг него
                    var zoom = control.querySelector(".zoom").value;
                    mapState = {
                        center: [lat, long],
                        zoom: zoom
                    };
                }
                else {
                    //параметры отображения карты определяются, исходя из местоположения пользователя
                    var bounds = centerObject.properties.get('boundedBy');
                    mapState = ymaps.util.bounds.getCenterAndZoom(
                        bounds,
                        [container.offsetWidth, container.offsetHeight]
                    );
                }
                mapState.controls = ["fullscreenControl", "geolocationControl", "typeSelector", "zoomControl"];
                var ymap = new ymaps.Map(container, mapState);

                var searchControl = new ymaps.control.SearchControl({
                    options: {
                        noPlacemark: true
                    }
                });
                searchControl.events.add('resultselect', function (e) {
                    var index = e.get('index');
                    searchControl.getResult(index).then(function (res) {
                        placePlacemark(res.geometry.getCoordinates());
                        var address = res.properties.get("text");
                        control.querySelector("textarea").value = address;
                        var hiddens = control.querySelectorAll("input[type=hidden][id*=_address_]");
                        for (var i = 0; i < hiddens.length; i ++) {
                            hiddens[i].value = address;
                        }
                    });
                })
                ymap.controls.add(searchControl);

                if (objectFound) {
                    //координаты уже заполнены и оказались корректными
                    placePlacemark([lat, long]);
                }

                ymap.events.add("click", function (e) {
                    placePlacemark(e.get("coords"));
                    control.querySelector("textarea").value = "";
                });

                ymap.events.add("boundschange", function (e) {
                    control.querySelector(".zoom").value = e.get("newZoom");
                });
                
                control.querySelector("[type=reset]").addEventListener("click", function (e) {
                    e.preventDefault();
                    reset(control);
                    ymap.geoObjects.remove(mark);
                    mark = undefined;
                });

                control.querySelector(".coord-lat").addEventListener("change", function (e) {
                    this.value = clearCoord(this.value);
                    makePlacemark();
                });

                control.querySelector(".coord-long").addEventListener("change", function (e) {
                    this.value = clearCoord(this.value);
                    makePlacemark();
                });

                var mark;
                function placePlacemark(coords) {
                    if (mark)
                    {
                        mark.geometry.setCoordinates(coords);
                    }
                    else {
                        mark = new ymaps.Placemark(coords, {}, {draggable: true});
                        mark.events.add("drag", function (e) {
                            showCoords(control, mark.geometry.getCoordinates());
                        });
                        ymap.geoObjects.add(mark);
                    }
                    showCoords(control, coords);
                }

                function clearCoord(input) {
                    var negSign = input[0] === "-" ? "-" : "";
                    var semiClearInput = input.replace(/[^\d.,]/g, "").replace(/,/g, ".");
                    var clearInput = semiClearInput.replace(".", "x").replace(/\./g, "").replace("x", ".");
                    var coord = negSign + clearInput;
                    return coord;
                }

                function makePlacemark() {
                    var lat = control.querySelector(".coord-lat").value;
                    var long = control.querySelector(".coord-long").value;
                    placePlacemark([lat, long]);
                    ymap.setCenter(mark.geometry.getCoordinates());
                }
            });
        })(control, lat, long, objectFound);
    };

    function showCoords(control, coords) {
        control.querySelector(".coord-lat").value = coords[0];
        control.querySelector(".coord-long").value = coords[1];
    }

    function reset(control) {
        control.querySelector(".coord-lat").value = "";
        control.querySelector(".coord-long").value = "";
        control.querySelector("textarea").value = "";
    }

    function isCoordCorrect(coordString) {
        return coordString && /^-?\d*(\.\d+)?$/.test(coordString);
    }

    var userPosition;
    function getUserPositionObject() {
        var deferred = new ymaps.vow.Deferred();
        if (userPosition) {
            deferred.resolve(userPosition);
        }
        ymaps.geolocation.get({provider: "yandex", mapStateAutoApply: true}).then(
            function (res) {
                deferred.resolve(res.geoObjects.get(0));
            }, function (err) {
                deferred.reject(err);
            }
        );
        return deferred.promise();
    }
});