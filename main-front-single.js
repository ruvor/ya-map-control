ymaps.ready(function () {
    var ymap = new ymaps.Map("ymap", {
        center: objectCoords,
        zoom: 13, //от 0 до максимального значения, зависящего от текущего положения карты
        controls: ["smallMapDefaultSet"]
    });
    var mark = new ymaps.Placemark(objectCoords);
    ymap.geoObjects.add(mark);
});